from .svg import Group, mm, mmU


class GraphicsItem:
    debug = False
    
    def __init__(self, width, height, stretch=1):
        self.width = mmU(width)
        self.height = mmU(height)
        self.stretch = stretch

    def draw(self, g, width, height):
        if self.debug:
            g.rect(mm(0.), mm(0.), width, height)


class Rect(GraphicsItem):
    def __init__(self, width, height, stretch=1, **args):
        GraphicsItem.__init__(self, width, height, stretch)
        self.args = args
    
    def draw(self, g, width, height):
        g.rect( mm(0.), mm(0.), width, height, **self.args )


class TextLabel(GraphicsItem):
    def __init__(self, width, height, text, fontSize=3, align="left", font=None, **kw):
        if not isinstance(text, str):
            raise TypeError("expected str, got %s" % type(text).__name__)
        
        super().__init__(width, height, **kw)
        self.text = text
        self.align = align
        self.fontSize = mmU(fontSize)
        self.fontFamily = font
    
    def draw(self, g: Group, width, height):
        #g.rect(0, 0, width, height, stroke="green")
        g.text(mm(0), mm(0), width, height, self.text, self.align, fontSize=self.fontSize, fontFamily=self.fontFamily)
