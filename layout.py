from .graphic import GraphicsItem, Rect, TextLabel
from .svg import Group, mm, mmU

__all__ = "Layout", "StackedLayout", "HBoxLayout", "VBoxLayout", "GridLayout", "Legend"


class Layout(GraphicsItem):
    def __init__(self, padding=0, margin=0, stretch=1, debug=False):
        self.padding = mmU(padding)
        self.margin = mmU(margin)
        self.stretch = stretch
        self.debugItems = debug


class StackedLayout(Layout):
    def __init__(self, **kw):
        super().__init__(**kw)
        self.items = list()
    
    def addItem(self, item):
        self.items.append(item)
    
    @property
    def width(self):
        return max( i.width for i in self.items ) + 2 * self.padding
        
    @property
    def height(self):
        return max( i.height for i in self.items ) + 2 * self.padding
    
    def draw(self, g, width, height):
        subG = g.group(self.padding, self.padding)
        for item in self.items:
            item.draw(subG, width-2*self.padding, height-2*self.padding)


class VBoxLayout(Layout):
    line = False

    def __init__(self, **kw):
        super().__init__(**kw)
        self.items = list()
    
    def addItem(self, item):
        self.items.append(item)
    
    @property
    def width(self):
        return max( i.width for i in self.items ) + 2 * self.padding
    
    @property
    def height(self):
        return sum( i.height for i in self.items ) + 2 * self.padding + self.margin * (len(self.items) - 1)
    
    def draw(self, g, width, height):
        y = self.padding
        
        for i, item in enumerate(self.items):
            if self.line and i > 0:
                lineY = y - self.margin / 2
                g.line(self.padding, lineY, width - self.padding, lineY)

            h = item.height
            if self.debugItems:
                g.rect( self.padding, y, width-2*self.padding, h, stroke="yellow" )
            
            subG = g.group(self.padding, y)
            item.draw(subG, width-2*self.padding, h)
            y += h + self.margin

        y -= self.margin

        if y > height:
            raise ValueError("no ySpace remaining: %f > %f" % (y, height))


class HBoxLayout(Layout):
    def __init__(self, **kw):
        super().__init__(**kw)
        self.items = list()
    
    def addItem(self, item):
        self.items.append(item)
    
    @property
    def width(self):
        return sum( i.width for i in self.items ) + 2 * self.padding + self.margin * (len(self.items) - 1)
    
    @property
    def height(self):
        return max( i.height for i in self.items ) + 2 * self.padding
    
    def draw(self, g, width, height):
        x = self.padding
        contentWidth = sum( item.width for item in self.items )
        sf = (width - contentWidth - (len(self.items)-1) * self.margin) / sum(item.stretch for item in self.items)
        
        for item in self.items:
            w = item.width + item.stretch * sf
            if self.debugItems:
                g.rect( x, self.padding, w, height-2*self.padding, stroke="yellow" )
                
            subG = g.group(x, self.padding)
            item.draw(subG, w, height-2*self.padding)
            x += w + self.margin


class GridLayout(Layout):
    emptyItem = GraphicsItem(0, 0)
    verticalLines = False
    horizontalLines = False

    def __init__(self, rows=0, columns=0, xmargin=0, ymargin=0, **kw):
        super().__init__(**kw)
        self.cols = columns
        self.rows = rows
        self.xmargin = mmU(xmargin)
        self.ymargin = mmU(ymargin)
        
        self.items = dict()
        self.rowstretch = dict()
        self.colstretch = dict()
    
    def get(self, col, row):
        return self.items.get((col, row), self.emptyItem)
    
    def setItem(self, item, row, column):
        assert item is not None

        if row + 1 > self.rows: self.rows = row + 1
        if column + 1 > self.cols: self.cols = column + 1
        
        self.items[(column, row)] = item

    def setRowStretch(self, row, stretch):
        self.rowstretch[row] = stretch

    def setColStretch(self, col, stretch):
        self.colstretch[col] = stretch

    @property
    def width(self):
        rows = self.rows
        cols = self.cols
        return sum( max( self.get(col, row).width for row in range(rows) ) for col in range(cols) ) + self.xmargin * (cols - 1)
    
    @property
    def height(self):
        rows = self.rows
        cols = self.cols
        return sum( max( self.get(col, row).height for col in range(cols) ) for row in range(rows) ) + self.ymargin * (rows - 1)
    
    def draw(self, g: Group, width, height):
        rows = self.rows
        cols = self.cols

        rowStretch = list(self.rowstretch.get(row, 1) for row in range(rows))
        colStretch = list(self.colstretch.get(col, 1) for col in range(cols))
        maxWidths = tuple( max( self.get(col, row).width for row in range(rows) ) for col in range(cols) )
        maxHeights = tuple( max( self.get(col, row).height for col in range(cols) ) for row in range(rows) )
        xSpace = (width - 2*self.padding - (cols-1) * self.xmargin - sum(maxWidths)) / sum(colStretch)
        ySpace = (height - 2*self.padding - (rows-1) * self.ymargin - sum(maxHeights)) / sum(rowStretch)
        
        if xSpace < 0:
            raise ValueError("no xSpace remaining for %s: %s" % (g, xSpace))
        
        if ySpace < 0:
            raise ValueError("no ySpace remaining: %s: %s" % (g, ySpace))

        if self.verticalLines:
            x = self.padding - self.xmargin / 2
            for col in range(1, cols):
                x += maxWidths[col-1] + xSpace * colStretch[col-1] + self.xmargin
                g.line(x, self.padding, x, height - self.padding)

        y = self.padding
        for row in range(rows):
            x = self.padding
            h = maxHeights[row] + ySpace * rowStretch[row]
            for col in range(cols):
                w = maxWidths[col] + xSpace * colStretch[col]

                if self.horizontalLines and row > 0:
                    hy = y - self.ymargin / 2
                    g.line(x, hy, x + w, hy)

                item = self.get(col, row)
                if item != self.emptyItem:
                    subG = g.group(x, y)
                    item.draw(subG, w, h)
                
                    if self.debugItems:
                        g.rect( x, y, w, h, stroke="yellow" )
                 
                x += w + self.xmargin
             
            y += h + self.ymargin


class Legend(GridLayout):
    entryWidth = mm(6)
    entryHeight = mm(3)
    entryTextWidth = mm(20)
    
    def __init__(self, entrys, margin=2, align="right", **kw):
        GridLayout.__init__(self, len(entrys), 2, xmargin=margin, ymargin=margin, **kw)

        if align == "left":
            symCol, textCol = 0, 1
        elif align == "right":
            symCol, textCol = 1, 0

        self.setColStretch(symCol, 0)
        for i, (color, text) in enumerate(entrys):
            self.setItem(Rect(self.entryWidth, self.entryHeight, fill=color), i, symCol)
            self.setItem(TextLabel(self.entryTextWidth, self.entryHeight, text, align=align), i, textCol)
