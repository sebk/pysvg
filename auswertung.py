from common.fragebogen import Section
from collections import Counter
from auswertung.fragen import fragen_lut


class TeilAuswertung:
    info = None
    
    def __init__(self, va, key, value):
        self.va = va
        self.key = key
        self.value = value
    
        self.data = dict()
        
        self.gesamt = 0
    
    def create(self, fragen):
        mapping = self.va.mapping
        for frage in fragen:
            o = fragen_lut[frage.name]
            if frage.id in mapping:
                i = o(frage, mapping[frage.id])
            else:
                i = o(frage)
            
            self.data[frage.id] = i
            
    def add(self, bogenId, werte):
        gesamtWertung = 0
        gesamtWert = 0
        isValid = True

        self.gesamt += 1
        
        for frageId, frageObj in self.data.items():
            v = werte.get(frageId)
            result = frageObj.add(v)

            if frageObj.wertung != None:
                if result is not False:
                    gesamtWertung += frageObj.wertung
                    gesamtWert += result * frageObj.wertung
                elif isValid:
                    if v is None:
                        print("  bogen %s: frage %s fehlt" % (bogenId, frageId))
                    else:
                        print("  bogen %s: ungültig durch frage %s (wert: %s)" % (bogenId, frageId, v))
                    isValid = False
        
        if isValid:
            return gesamtWert, gesamtWertung
        else:
            return False
    
    def finish(self):
        pass

    def checkOutliers(self, bögen):
        checks = dict()
        for frageId, frageObj in self.data.items():
            check = frageObj.outliers()
            if check: checks[frageId] = check

        def check(werte):
            for frageId, check in checks:
                check(werte.get(frageId))


class HauptAuswertung(TeilAuswertung):
    def __init__(self, va):
        TeilAuswertung.__init__(self, va, None, None)
        self.bögen = list()
        self.objektivNoten = Counter()
        self.subjektivNoten = Counter()
    
    def add(self, bogenId, werte):
        r = super().add(bogenId, werte)
        if r != False:
            gesamtWert, gesamtWertung = r

            obj = 15 * gesamtWert / gesamtWertung
            subj = int(werte["20"])
            self.bögen.append((obj, subj))

            self.objektivNoten[int(obj+0.5)] += 1
            self.subjektivNoten[subj] += 1
    
    def finish(self):
        if not len(self.bögen):
            print("Hauptauswertung: keine Bögen")
            return
        
        score = sum((o + s)/2 for o, s in self.bögen)/ len(self.bögen)
        var = sum((o + s)**2/4 for o, s in self.bögen ) / len(self.bögen) - score**2

        self.info = {
            "bögen":            self.gesamt,
            "gültig":           len(self.bögen),
            "subjektiv":        sum(s for o, s in self.bögen) / len(self.bögen),
            "objektiv":         sum(o for o, s in self.bögen) / len(self.bögen),
            "wertung":          score,
            "wertung_s":        var**0.5,
            "objektivnoten":    self.objektivNoten,
            "subjektivnoten":   self.subjektivNoten
        }

class AuswertungsAbschnitt:
    def __init__(self, va, frageKey):
        self.va = va
        self.frageKey = frageKey
        self.teile = dict()
        self.fragen = list()
    
    def create(self, entryId=None, teil=None):
        ta = teil or TeilAuswertung(self.va, self.frageKey, entryId)
        ta.create(self.fragen)
        self.teile[entryId] = ta
        return ta
    
    def add(self, bogen):
        werte = bogen["werte"]
        
        if self.frageKey is None:
            entryId = None
        else:
            entryIdR = werte.get(self.frageKey)
            if entryIdR is None: return False
            
            entryId = self.va.resolveValueRef(self.frageKey, entryIdR)
        
        try: teil = self.teile[entryId]
        except KeyError: teil = self.create(entryId)
        
        return teil.add(bogen["id"], werte)
    
    def finish(self):
        for ta in self.teile.values():
            ta.finish()
    
    def __str__(self):
        return "<%s %s>" % (self.__class__.__name__, self.frageKey)

class VorlesungsAuswertung:
    def __init__(self, fragebogen, mapping):
        self.mapping = mapping
        self.gesamt = 0
        
        self.hauptauswertung = HauptAuswertung(self)
        
        # key -> AuswertungsAbschnitt
        self.abschnitte = dict()
        
        # frageId -> key
        self.fragenKeys = dict()
        
        abschnitt = self.hauptauswertung
        for item in fragebogen.items():
            if isinstance(item, Section):
                key = item.key
                try:
                    abschnitt = self.abschnitte[key]
                except KeyError:
                    abschnitt = self.abschnitte[key] = AuswertungsAbschnitt(self, key)
            
            else:
                abschnitt.fragen.append(item)
        
        self.abschnitte[None].create(teil=self.hauptauswertung)

    def resolveValueRef(self, frageId, value):
        e = self.mapping[frageId]
        
        #print(frageId, value, end=" -> ")
        seen = {value}
        while isinstance(value, int):
            value = e[value]
            assert value not in seen, self.mapping
            seen.add(value)
        
        #print(value)
        return value
    
    def add(self, bogen):
        self.gesamt += 1
        for frageKey, abschnitt in self.abschnitte.items():
            abschnitt.add(bogen)
    
    def finish(self):
        for a in self.abschnitte.values():
            a.finish()
