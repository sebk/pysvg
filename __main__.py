import os, os.path
import common.conf as conf
from common.files import cleanname
from common.fragebogen import fragebogen01
from common.remote import connectSynchronus
from auswertung.auswertung import VorlesungsAuswertung
from auswertung.seiten import aushang, übungsgruppe, details
from auswertung.poster import PosterUnit, Poster
from auswertung.evalu import Evaluation
import csv
from pathlib import Path
from auswertung.ausdruck import Ausdruck, TempAusdruck
from auswertung.svg import TempSVG, SVG
from tempfile import TemporaryFile
from PyPDF2 import PdfFileMerger
import subprocess

try:
    import cairosvg.surface
except: pass

inkscape = subprocess.Popen(["inkscape", "--shell"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
inkscape.stdout.readline()
inkscape.stdout.read(1)

def svg2pdf(svgFile: Path, pdfFile: Path):
    cmd = "-A %r %r\n" % (str(pdfFile), str(svgFile))
    inkscape.stdin.write(cmd.encode())
    inkscape.stdin.flush()
    inkscape.stdout.read(1)


class PdfWriter:
    def __init__(self, filename: Path):
        self.filename = filename
        self.merger = PdfFileMerger()

        from auswertung.layout import VBoxLayout
        self.emptyPage = TempAusdruck()
        self.emptyPage.addPage(None)
        self.emptyPage.setLayout(VBoxLayout())
        self.emptyPage.closePage()

    def close(self):
        self.merger.write(self.filename.open("wb"))
        self.merger.close()

    def addSvg(self, svg: SVG, bookmark=None):
        if isinstance(svg, TempSVG):
            data = svg.data()
            f = TemporaryFile()
            cairosvg.surface.PDFSurface.convert(data, write_to=f)
        else:
            data = svg.path.open("rb").read()
            pdfFile = svg.path.with_suffix(".pdf")
            svg2pdf(svg.path, pdfFile)
            f = pdfFile.open("rb")

        self.merger.append(f, bookmark=bookmark)

    def addAusdruck(self, ausdruck: Ausdruck, title=None):
        if ausdruck is None: return
        for i, svg in enumerate(ausdruck.pages):
            self.addSvg(svg, bookmark=None if i else title)

    def addEmpty(self):
        self.addAusdruck(self.emptyPage)

    def addWriter(self, writer: Path, title=None):
        self.merger.append(writer.filename.open("rb"), bookmark=title)


def main(makePDF=False, makePoster=False, semester=None):
    root = connectSynchronus()
    semesterListe = root.semester
    dozentenListe = root.dozenten
    
    if semester is not None:
        semesterId = semesterListe.ident(semester)
        semesterName = semesterListe.name(semesterId)
    else:
        semesterId, semesterName = semesterListe.semesterJetzt()
    
    vorlesungsListe = semesterListe.vorlesungen(semesterId)
    evaluListe = semesterListe.evalus(semesterId)
    
    vorlesungen = dict((vorlesungsListe.items()))
    dozenten = dict((dozentenListe.items()))

    destdir = Path(conf.datadir) / ("%4i-%s" % (semesterId // 10, ("SS", "WS")[semesterId % 10])) / "auswertung"
    if not destdir.exists():
        destdir.mkdir()

    """
    evalus = list(
        Evaluation(
            evaluId,
            dozenten[e["dozent"]],
            vorlesungen[e["vorlesung"]],
            evaluListe)
        for evaluId, e in (yield from evaluListe.items())
    )
    """

    if makePDF:
        gesamtPostPDF = PdfWriter(destdir / "post.pdf")

    if makePoster:
        poster = Poster(semesterName, destdir)

    vorlesungsErgebnisse = list()
    übungsgruppenErgebnisse = list()
    for evaluId, evalu in sorted(evaluListe.items(), key=lambda t: t[1]["dozent"]):
        vorlesung = vorlesungen[evalu["vorlesung"]]
        dozent = dozenten[evalu["dozent"]]
        bögen = evaluListe.bögen(evaluId)
        mapping = evaluListe.mapping(evaluId)
        
        print("%r [%s: %s]" % (evaluId, vorlesung["titel"], dozent["nachname"]))

        if makePDF:
            dirname = destdir / cleanname("%s-%s" % (dozent["nachname"], vorlesung["titel"]))
            if not dirname.exists():
                dirname.mkdir()
        
        auswertung = VorlesungsAuswertung(fragebogen01, mapping)
        for bogenId, bogen in bögen.items():
            try:
                auswertung.add(bogen)
            except:
                print("Bogen", bogenId)
                raise

        auswertung.finish()

        if makePDF:
            postPrinter = PdfWriter(dirname / "post.pdf")

            a = aushang(auswertung, dirname, vorlesung, semesterName, evalu, dozent)
            b = details(auswertung, dirname, vorlesung, semesterName, evalu, dozent)

            postPrinter.addAusdruck(a, "Vorlesungsbewertung")
            postPrinter.addAusdruck(b, "Details")
            if b: postPrinter.addEmpty()

        if makePoster:
            poster.add(PosterUnit(evalu, vorlesung, dozent, auswertung))
        
        for üg in auswertung.abschnitte["21"].teile.values():
            note = üg.data["31"]
            if note.valid < 2: continue
            if makePDF:
                ü = übungsgruppe(üg, dirname, vorlesung, semesterName, evalu, dozent)
                postPrinter.addAusdruck(ü, "ÜG: %s" % üg.value)
            
            if note.mean is not None:
                übungsgruppenErgebnisse.append((note.mean, note, üg, vorlesung))

        if makePDF:
            postPrinter.close()

            title = "%s : %s" % (dozent["nachname"], vorlesung["titel"])
            gesamtPostPDF.addWriter(postPrinter, title=title)

        i = auswertung.hauptauswertung.info
        if i is not None:
            vorlesungsErgebnisse.append((i["wertung"], i["wertung_s"], i["gültig"], i["bögen"],
                                        vorlesung["titel"], "{titel} {vorname} {nachname}".format(**dozent)))


    sorthelper = lambda l: l[0]
    fmt = lambda v: "-" if v is None else "%.3f" % v
    vorlesungsErgebnisse.sort(key=sorthelper, reverse=True)
    übungsgruppenErgebnisse.sort(key=sorthelper, reverse=True)
    
    print("Vorlesungen")
    for mittel, stdabw, gültig, bögen, vltitel, dozent in vorlesungsErgebnisse:
        print("%6s +- %5s [%3i/%3i] %30s %60s" % (fmt(mittel), fmt(stdabw), gültig, bögen, dozent[:30], vltitel[:60]))
    
    
    ügGültig, ügUngültig = [], []
    for e in übungsgruppenErgebnisse:
        mean, note, üg, vorlesung = e
        (ügGültig if note.valid >= 8 else ügUngültig).append(e)
    
    def printÜg(e):
        mean, note, üg, vorlesung = e
        print("%6s +- %5s [%3i/%3i] %30s %60s" % (fmt(note.mean), fmt(note.stdev), note.valid, note.gesamt, üg.value[:30], vorlesung["titel"][:60]))
    
    def ügRow(e):
        mean, note, üg, vorlesung = e
        return fmt(note.mean), fmt(note.stdev), note.valid, note.gesamt, üg.value, vorlesung["titel"]
        
    print("Übungsgruppen")
    for e in ügGültig: printÜg(e)
    print("-"*80)
    for e in ügUngültig: printÜg(e)
    
    with (destdir / "vorlesungen.csv").open("w") as f:
        w = csv.writer(f, csv.unix_dialect)
        w.writerow(("Note", "Standardabweichung", "Gültige Bögen", "Rücklauf", "Vorlesung", "Dozent"))
        w.writerows(vorlesungsErgebnisse)

    with (destdir / "übungsgruppen.csv").open("w") as f:
        w = csv.writer(f, csv.unix_dialect)
        w.writerow(("Note", "Standardabweichung", "Gültige Bögen", "Rücklauf", "Übungsgruppe", "Vorlesung"))
        
        for e in ügGültig: w.writerow(ügRow(e))
        for e in ügUngültig: w.writerow(ügRow(e))

    if makePDF:
        gesamtPostPDF.close()

    if makePoster:
        poster.finish(destdir)
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='PyEvalu Auswertung')
    parser.add_argument("--poster", action="store_true", help="Poster erstellen")
    parser.add_argument("--pdf", action="store_true", help="PDF erstellen")
    parser.add_argument("--semester")
    args = parser.parse_args()

    print(args)
    main(makePoster=args.poster, makePDF=args.pdf, semester=args.semester)
