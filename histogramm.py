from .graphic import GraphicsItem
from .svg import mm, mmU, Color

__all__ = "HistogrammEntry", "HistogrammMultiEntry", "HistogrammSeperator", "Histogramm"


class HistogrammSeperator:
    bins = 0
    label = None
    
    def maxValue(self): return 0

    def entrys(self): return tuple()


class HistogrammEntry:
    bins = 1
    defaultFill = Color(1, 1, 1) * 0.7
    def __init__(self, value, valueText=None, label=None, labelAlign="center", color=defaultFill):
        self.value = value
        self.valueText = str(value) if valueText is None else valueText
        self.label = label
        self.labelAlign = labelAlign
        self.color = str(color)
    
    def maxValue(self): return self.value

    def entrys(self):
        return [ (self.value, self.valueText, self.color) ]


class HistogrammMultiEntry:
    defaultFill = Color(1, 1, 1) * 0.7

    def __init__(self, values, valueTexts=None, label=None, labelAlign="center", colors=None):
        self.bins = len(values)
        self.values = values
        
        if valueTexts is None:
            self.valueTexts = map(str, values)
        else:
            assert len(valueTexts) == self.bins
            self.valueTexts = valueTexts
        
        self.label = label
        self.labelAlign = labelAlign
        
        if colors is None:
            self.colors = [self.defaultFill] * self.bins
        else:
            assert len(colors) == self.bins
            self.colors = colors
    
    def maxValue(self): return max(self.values)

    def entrys(self):
        return zip( self.values, self.valueTexts, self.colors )


class Histogramm(GraphicsItem):
    binWidth = mm(4) # width of one bin
    binSpacing = mm(3) # space between bins
    margin = mm(2) # border spacing
    keyHeight = mm(5) # height of textfield for labels
    titleHeight = mm(5) # height of textfield for title
    titleAlign = "center"
    binHeight = mm(30) # height for bins
    numberHeight = mm(5) # height of textfield for bin numbers
    frame = True
    labelSize = mm(2)
    
    def __init__(self, title, entrys, legend=None):
        numBins = sum( entry.bins for entry in entrys )
        
        width = 2 * self.margin + numBins * (self.binWidth + self.binSpacing) + self.binSpacing
        height = 2 * self.margin + self.keyHeight + self.binHeight + self.numberHeight

        extraHeight = mm(0)
        if title is not None:
            extraHeight = self.margin + self.titleHeight
        if legend is not None:
            extraHeight = max(extraHeight, legend.height + self.margin)

        height += extraHeight

        GraphicsItem.__init__(self, width, height)
        
        self.title = title
        self.entrys = entrys
        self.legend = legend
    
    def draw(self, g, width, height):
        if self.frame:
            g.rect(mm(0.), mm(0.), width, self.height)

        if self.title is not None:
            g.text(self.margin, self.margin, width - 2 * self.margin, self.titleHeight, self.title, self.titleAlign, fontSize=mm(3))
        
        if self.legend:
            h = self.legend.height
            self.legend.draw( g.group( self.margin, self.margin ), width - 2 * self.margin, h )

        maxValue = max( max( entry.maxValue() for entry in self.entrys ), 1 ) if len(self.entrys) else 1
        
        numBins = sum( entry.bins for entry in self.entrys )
        
        binSpacing = (width - 2 * self.margin - self.binWidth * numBins) / (len(self.entrys) + 1)
        x0 = self.margin + binSpacing
        for i, entry in enumerate(self.entrys):
            binX0, binWidth = x0, self.binWidth * entry.bins
            j = 0
            for value, text, fill in entry.entrys():
                height = self.binHeight * value / maxValue
                y = self.height - self.margin - self.keyHeight - height
                if height:
                    g.rect(x0, y, self.binWidth, height, fill=fill, stroke="black")
                
                if text:
                    g.text(
                        x0,
                        y - self.numberHeight,
                        self.binWidth,
                        self.numberHeight,
                        text,
                        "center",
                        "mid",
                    )
                x0 += self.binWidth
                
                j += 1
            
            # draw Label
            if entry.label:
                g.text(binX0,
                       self.height - self.margin - self.keyHeight,
                       binWidth,
                       self.keyHeight,
                       entry.label,
                       entry.labelAlign,
                       "mid",
                       fontSize=self.labelSize
                )
            x0 += binSpacing
        
        g.hline(self.margin,
            self.height - self.margin - self.keyHeight,
            width - self.margin
        )
