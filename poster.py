__author__ = 'sebk'

from auswertung.auswertung import VorlesungsAuswertung
from auswertung.ausdruck import Ausdruck
from auswertung.layout import *
from auswertung.graphic import TextLabel
from auswertung.seiten.poster import poster
from auswertung.svg import U
from pathlib import Path
from math import floor, ceil


class PosterUnit:
    def __init__(self, evalu, vorlesung, dozent, auswertung: VorlesungsAuswertung):
        self.evalu = evalu
        self.vorlesung = vorlesung
        self.dozent = dozent
        self.auswertung = auswertung

        self.layout = None

    @property
    def size(self):
        if self.layout is None:
            self.layout = poster(self.auswertung, self.vorlesung, self.evalu, self.dozent)
            self._size = self.layout.width, self.layout.height

        return self._size
"""
    def kategorie(self):
        kv = self.evalu.get("kursvorlesung"):
        if kv is None:

        if kv == False
"""

class Poster:
    pageHeight = U(42, "in")
    maxPageWidth = U(900, "mm")
    headerFont = "URW Gothic L"

    def __init__(self, semesterName, folder):
        self.units = list()
        self.rows = None
        self.columns = None
        self.semesterName = semesterName
        self.ausdruck = Ausdruck(folder)

    def add(self, unit: PosterUnit):
        if unit.auswertung.hauptauswertung.info:
            self.units.append(unit)

    def makePage(self, page, units):
        topLine = HBoxLayout(margin=0, padding=0)
        topLine.addItem(TextLabel(
            200, 6,
            "Ergebnisse der Evaluation im %s" % self.semesterName,
            align="left", fontSize=6, font=self.headerFont))
        topLine.addItem(TextLabel(
            100, 6,
            "Dozenten: %s … %s" % (units[0].dozent["nachname"], units[-1].dozent["nachname"]),
            align="right", fontSize=6, font=self.headerFont))

        vlayout = VBoxLayout(margin=10, padding=10)
        vlayout.line = True
        vlayout.addItem(topLine)

        cols = int(ceil(len(units) / self.rows))
        grid = GridLayout(self.rows, cols, padding=0, ymargin=8, xmargin=10)
        grid.verticalLines = True
        grid.horizontalLines = True

        for i, unit in enumerate(units):
            grid.setItem(unit.layout, i % self.rows, (i % (self.rows * cols)) // self.rows)

        vlayout.addItem(grid)

        self.ausdruck.addPage("poster_%i" % page, (None, self.pageHeight))
        self.ausdruck.setLayout(vlayout)
        self.ausdruck.closePage()

    def ranking(self, accept, score, N=3, minDiff=0.1):
        results = []
        lower = None
        for unit in sorted(self.units, key=score):
            if not accept(unit): continue

            results.append(unit)
            n = len(results)
            if n < N: continue
            elif n == N:
                lower = score(unit)
            elif score(unit) + minDiff > lower:
                results.append(unit)
            else:
                break

        return results




    def finish(self, destdir: Path):
        self.units.sort(key=lambda unit: unit.dozent["nachname"])
        #score = lambda u: u.hauptauswertung.info["wertung"]
        #valid = lambda u: u.hauptauswertung.info["gültig"]
        #kursvorlesungen = self.ranking(lambda u: u.kategorie == "kursvorlesung" and valid(u) >= 16, score)
        #spezialvorlesungen = self.ranking(lambda u: u.kategorie == "spezialvorlesung" and valid(u) >= 10, score)


        unitWidth = max(unit.size[0] for unit in self.units)
        unitHeight = max(unit.size[1] for unit in self.units)

        self.rows = int(floor((self.pageHeight) / unitHeight))
        print("%i rows (%4.1f%% loss)" % (self.rows, 100 * (1. - (unitHeight * self.rows) / self.pageHeight)))

        maxColumns = int(floor(self.maxPageWidth / unitWidth))

        # we need at least this many separate pages
        pages = int(ceil(len(self.units) / (maxColumns * self.rows)))

        # how many rows do we actually need?
        self.columns = int(ceil(len(self.units) / (pages * self.rows)))

        ausdruck = Ausdruck(destdir)
        # distribute the units across the pages
        for page in range(int(pages)):
            print("generating page %i" % page)
            NM = self.rows * self.columns
            self.makePage(page, self.units[page * NM : (page + 1) * NM])
