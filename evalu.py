__author__ = 'sebk'
from common.remote import Proxy
from auswertung.auswertung import VorlesungsAuswertung
from common.fragebogen import fragebogen01
import asyncio


class Evaluation:
    def __init__(self, evaluId, dozent, vorlesung, evaluliste: Proxy):
        self.evaluId = evaluId
        self.dozent = dozent
        self.vorlesung = vorlesung
        self._evaluliste = evaluliste



    @asyncio.coroutine
    def run(self):
        bögen = yield from self._evaluliste.bögen(self.evaluId)
        mapping = yield from self._evaluliste.mapping(self.evaluId)
        self.auswertung = VorlesungsAuswertung(fragebogen01, mapping)

        for bogenId, bogen in (yield from bögen.items()):
            try:
                self.auswertung.add(bogen)
            except:
                print("Bogen", bogenId)
                raise

        self.auswertung.finish()
