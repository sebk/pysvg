from auswertung.graphic import GraphicsItem
from auswertung.histogramm import Histogramm, HistogrammEntry, HistogrammSeperator
from auswertung.svg import Color
from collections import Counter

class Frage:
    def __init__(self, f=None):
        self.id = f.id
        self.wertung = f.attr.get("wertung")
    
    def graphic(self):
        return GraphicsItem(10, 10)
    
    """ returns:
        None    Wenn kein Wert übergeben wurde und die Frage nicht in die Wertung eingeht
        False   wenn der Wert ungültig ist, oder kein Wert angegeben wurde und die Frage in die Wertung eingeht
        float   wenn gültig. (dann die entsprechende Wertung zwischen 0 und 1
    """
    def add(self, value: str):
        pass

    def outliers(self):
        pass

class FrageStatistikDiskret(Frage):
    
    def __init__(self, f):
        Frage.__init__(self, f)
        
        self.vmin = f.attr.get("min")
        self.vmax = f.attr.get("max")
        self.optimum = f.attr.get("optimum")
        
        self.stat = Counter()
        self.valid = 0
        self.invalid = 0
        self.empty = 0
    
    def add(self, value):
        if value is None:
            self.empty += 1
            return None if self.wertung is None else False
        
        v = int(value)
        if (self.vmin and v < self.vmin) or (self.vmax and v > self.vmax):
            self.invalid += 1
            return False
        
        self.stat[v] += 1
        self.valid += 1
            
        if self.optimum is not None:
            if v == self.optimum:
                return 1.0
            elif v < self.optimum:
                return (v - self.vmin) / (self.optimum - self.vmin)
            elif v > self.optimum:
                return (self.vmax - v) / (self.vmax - self.optimum)
        else:
            return True

    @property
    def mean(self):
        n = sum(self.stat.values())
        if n < 1:
            return None
        else:
            return sum(k*v for k, v in self.stat.items()) / n

    @property
    def stdev(self):
        n = sum(self.stat.values())
        mean = self.mean

        if n < 2:
            return None
        else:
            return (sum(v**2 * k for v, k in self.stat.items()) / n - mean**2)**0.5

    def avgFormat(self, fmt, default):
        mean = self.mean
        if mean is None:
            return default

        return fmt % (mean, self.stdev or 0.)

    @property
    def gesamt(self):
        return self.empty + self.invalid + self.valid

    def outliers(self):
        mean = self.avg()
        s = self.stddev()**0.5

        upper = mean + 2 * s
        lower = mean - 2 * s

        def check(value):
            if value is None: return
            v = int(value)
            if (self.vmin and v < self.vmin) or (self.vmax and v > self.vmax): return
            return lower < v < upper

    def graphic(self, title=None, histogrammClass=Histogramm, skip=True):
        entrys = [ HistogrammEntry(self.empty, label="k.A.", color="white") ]

        if self.valid and not skip:
            vmin = min(self.stat.values()) if self.vmin is None else self.vmin
            vmax = max(self.stat.values()) if self.vmax is None else self.vmax
            for key in range(vmin, vmax+1):
                entrys.append( HistogrammEntry(
                    value=self.stat[key],
                    label=str(key)
                ) )
        elif skip:
            prevKey = None
            for key in sorted(self.stat.keys()):
                if prevKey is not None and key > prevKey + 1:
                    entrys.append(HistogrammSeperator())

                entrys.append( HistogrammEntry(
                    value=self.stat[key],
                    label=str(key)
                ) )

                prevKey = key

        return histogrammClass(title, entrys)

class Ankreuzfrage(FrageStatistikDiskret):
    def __init__(self, f):
        Frage.__init__(self, f)
        
        self.vmin = 1
        self.vmax = f.attr["möglichkeiten"]
        self.optimum = f.attr.get("optimum")
        
        self.stat = dict( (k, 0) for k in range( self.vmin, self.vmax + 1 ) )
        self.valid = 0
        self.invalid = 0
        self.empty = 0
        
        self.bedeutungen = f.attr["bedeutung"]
        self.title = f.text
    
    def graphic(self, title=None, histogrammClass=Histogramm, labelOverride={}):
        entrys = [ HistogrammEntry(self.empty, label="k.A.", color="white") ]
        for key in range(self.vmin, self.vmax+1):
            if key in self.bedeutungen:
                label = labelOverride.get(key) or self.bedeutungen[key]
                if key == self.vmin:
                    align = "left"
                elif key == self.vmax:
                    align = "right"
                else:
                    align = "center"
            else:
                label = None
                align = None

            if self.optimum:
                score = 1.0 - abs(key - self.optimum) / max(abs(self.optimum - self.vmin), abs(self.optimum - self.vmax))
                color = Color.fromHue(score / 3) * 0.9
            else:
                color = HistogrammEntry.defaultFill

            entrys.append( HistogrammEntry(
                value=self.stat[key],
                label=label,
                labelAlign=align,
                color=color
            ) )
        
        return histogrammClass(title, entrys)
    
class FrageStatistikReal(Frage):
    def __init__(self, f):
        Frage.__init__(self, f)
        
        self.vmin = f.attr.get("min")
        self.vmax = f.attr.get("max")
        
        self.sum = 0.0
        self.squareSum = 0.0
        self.count = 0
        self.values = list()
        self.empty = 0
    
    def add(self, value):
        if value is None:
            self.empty += 1
            return None if self.wertung is None else False
        
        v = float(value)
        if self.vmin != None and v < self.vmin:
            return False
        
        if self.vmax != None and v > self.vmax:
            return False
            
        self.count += 1
        self.sum += v
        self.squareSum += v**2
        self.values.append(v)
        
        if self.vmin != None and self.vmax != None:
            return (v - self.vmin) / (self.vmax - self.vmin)
   
    def avg(self):
        return self.sum / self.count
    
    def graphic(self, title, ranges, histogrammClass=Histogramm):
        bins = [0] * (len(ranges)+1)
        for v in self.values:
            b = 0
            for r in ranges:
                if v < r:
                    bins[b] += 1
                    break
                b += 1
            else:
                bins[b] += 1
        
        begin = ranges[0]
        entrys = [ HistogrammEntry(value=bins[0], label="<%s" % begin) ]
        
        index = 1
        for end in ranges[1:]:
            text = "[%s,%s)" % (begin, end)
            
            entrys.append( HistogrammEntry(
                value=bins[index],
                label="[%i,%i)" % (begin, end)
            ) )
            
            begin = end
            index += 1
            
        entrys.append(HistogrammEntry(value=bins[-1], label="≥%s" % end))
        
        return histogrammClass(title, entrys)


class Auswahlfrage(Frage):
    def __init__(self, f, emap=None):
        Frage.__init__(self, f)
        
        if "werte" in f.attr:
            w = f.attr["werte"]
            self.werte = dict( (i+1, v) for i, v in enumerate(w) )
            if emap: self.werte.update(emap)
        elif emap:
            self.werte = emap
        
        self.stat = Counter()
    
        self.missing = 0
    
    def add(self, value):
        if value in (None, False):
            self.missing += 1
            return
        
        while isinstance(value, int):
            value = self.werte[value]
        
        self.stat[value] += 1

def ZahlFrage(f):
    if f.attr["typ"] == "zahl":
        return FrageStatistikReal(f)
    elif f.attr["typ"] == "ganzzahl":
        return FrageStatistikDiskret(f)
    else:
        raise NotImplementedError(f.attr)

class TextFrage(Frage):
    def add(self, value):
        pass

fragen_lut = {
    "ankreuzfrage":     Ankreuzfrage,
    "textfrage":        TextFrage,
    "zahlfrage":        ZahlFrage,
    "auswahlfrage":     Auswahlfrage
}
