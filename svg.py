import xml.dom.minidom
from pathlib import Path
from gzip import GzipFile
from functools import partial


class U:
    _factors = {
        "mm":   0.001,
        "m":    1.0,
        "cm":   0.01,
        "in":   0.0254
    }

    def __init__(self, value, suffix):
        self.value = value
        self.suffix = suffix

    def to(self, suffix):
        if suffix == self.suffix:
            return self.value
        else:
            return self.value * self._factors[self.suffix] / self._factors[suffix]

    def conv(self, suffix):
        return U(self.to(suffix), suffix)

    def __str__(self):
        return "%s%s" % (self.value, self.suffix)

    def __repr__(self):
        return "U(%r, %r)" % (self.value, self.suffix)

    def __mul__(self, other):
        return U(self.value * other, self.suffix)

    __rmul__ = __mul__

    def __add__(self, other):
        if other == 0:
            return self
        else:
            return U(self.value + other.to(self.suffix), self.suffix)

    __radd__ = __add__

    def __sub__(self, other):
        if other == 0:
            return self
        else:
            return U(self.value - other.to(self.suffix), self.suffix)

    def __rsub__(self, other):
        if other == 0:
            return U(-self.value, self.suffix)
        else:
            return U(other.to(self.suffix) - self.value, self.suffix)

    def __neg__(self):
        return U(-self.value, self.suffix)

    def __truediv__(self, other):
        if isinstance(other, U):
            return self.value / other.to(self.suffix)
        else:
            return U(self.value / other, self.suffix)

    def __gt__(self, other):
        if isinstance(other, U):
            return self.value > other.to(self.suffix)
        elif other == 0:
            return self.value > 0
        else:
            return TypeError

    def __lt__(self, other):
        if isinstance(other, U):
            return self.value < other.to(self.suffix)
        elif other == 0:
            return self.value < 0
        else:
            return TypeError

    def __bool__(self):
        return bool(self.value)

mm = lambda v: U(v, "mm")

def defaultUnit(suffix, value):
    if isinstance(value, U):
        return value
    else:
        return U(value, suffix)

mmU = partial(defaultUnit, "mm")

A4 = (mm(210), mm(297)) # mm
A2 = A4[0] * 2, A4[1] * 2
A0 = A2[0] * 2, A2[1] * 2


def _u(u: U):
    assert isinstance(u, U)
    return str(u)


def cssFormat(style: dict):
    return ";".join("%s:%s" % (k, v) for k, v in style.items() if v is not None)


class Color:
    def __init__(self, r=0, g=0, b=0):
        assert 0 <= r <= 1
        self.r = r
        assert 0 <= g <= 1
        self.g = g
        assert 0 <= b <= 1
        self.b = b

    @staticmethod
    def fromHue(hue: float):
        assert 0 <= hue <= 1
        hue *= 6
        if hue < 1:
            return Color(1, hue, 0)
        elif hue < 2:
            return Color(2-hue, 1, 0)
        elif hue < 3:
            return Color(0, 1, hue-2)
        elif hue < 4:
            return Color(0, 4-hue, 1)
        elif hue < 5:
            return Color(hue-4, 0, 1)
        else:
            return Color(1, 0, 6-hue)

    def __mul__(self, other):
        if isinstance(other, float):
            if other <= 1:
                return Color(self.r * other, self.g * other, self.b * other)
            else:
                k = 1 / other
                return Color(1 - (1-self.r) * k, 1 - (1-self.g) * k, 1 - (1-self.b) * k)
        elif isinstance(other, Color):
            return Color(self.r * other.r, self.g * other.g, self.b * other.b)
        else:
            raise NotImplemented

    def __add__(self, other):
        if isinstance(other, float):
            return Color(self.r + other, self.g + other, self.b + other)

    def __str__(self):
        return "rgb(%i,%i,%i)" % (int(255 * self.r), int(255 * self.g), int(255 * self.b))

red = Color(r=1)
green = Color(g=1)
blue = Color(b=1)
white = Color(1, 1, 1)
black = Color(0, 0, 0)


class Group:
    def __init__(self, document, element, toVP):
        self.doc = document
        self.g = element
        self.toVP = toVP
    
    def element(self, name, **args):
        e = self.doc.createElement(name)
        for k, v in args.items():
            if k != None:
                e.setAttribute(k, v)
        
        self.g.appendChild(e)
        return e
        
    def rect(self, x, y, width, height, fill="none", stroke="black", strokeWidth=mm(0.1)):
        r = self.element(
            "rect",
            x=self.toVP(x),
            y=self.toVP(y),
            width=self.toVP(width),
            height=self.toVP(height),
            fill=str(fill),
            stroke=stroke
        )
        r.setAttribute("stroke-width", self.toVP(strokeWidth))

    def group(self, x, y):
        if x or y:
            return Group(
                self.doc,
                self.element("g", transform=("translate(%s %s)" % (self.toVP(x), self.toVP(y)))),
                self.toVP
            )
        else:
            return self
    
    def text(self, x, y, width, height, text, align="left", valign="top", fontSize=mm(3), fontFamily=None):
        lines = text.split("\n")
        
        if align == "left":
            textAnchor = "start"
            tx = x
        elif align == "right":
            textAnchor = "end"
            tx = x + width
        else:
            textAnchor = "middle"
            tx = x + width/2
        
        if valign == "top":
            ty = y + fontSize
        elif valign == "mid":
            ty = y + height/2 + len(lines)*fontSize/2
        elif valign == "bottom":
            ty = y + height + (1-len(lines))*fontSize
            
        for line in lines:
            t = self.element(
                "text",
                x=self.toVP(tx),
                y=self.toVP(ty),
                width=self.toVP(width),
                height=self.toVP(height))
            t.setAttribute("style", cssFormat({
                "font-size": self.toVP(fontSize),
                "text-anchor": textAnchor,
                "font-family": fontFamily
            }))
            tn = self.doc.createTextNode(line)
            t.appendChild(tn)
            ty += fontSize
    
    def line(self, x0, y0, x1, y1, stroke="black", strokeWidth=mm(0.1) ):
        l = self.element(
            "line",
            x1=self.toVP(x0),
            y1=self.toVP(y0),
            x2=self.toVP(x1),
            y2=self.toVP(y1), stroke=stroke)
        l.setAttribute("stroke-width", self.toVP(strokeWidth))
    
    def hline(self, x0, y0, x1, **kw):
        self.line(x0, y0, x1, y0, **kw)
    
    def vline(self, x0, y0, y1, **kw):
        self.line(x0, y0, x0, y1, **kw)

svgCSS = """
text {
  font-family: 'Liberation Serif';
}
"""

class SVG(Group):
    def __init__(self, path: Path):
        self.path = path
        impl = xml.dom.minidom.DOMImplementation()
        doctype = impl.createDocumentType(
            "svg",
            "-//W3C//DTD SVG 1.0//EN",
            "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd"
        )
        
        self.doc = impl.createDocument(
            "http://www.w3.org/2000/svg",
            "svg",
            doctype
        )

        self.viewportUnit = "mm"

        self.g = self.doc.documentElement
        self.g.setAttribute("xmlns", "http://www.w3.org/2000/svg")

        self.element("style").appendChild(self.doc.createTextNode(svgCSS))

    def toVP(self, u: U):
        return str(u.to(self.viewportUnit))

    def setSize(self, width, height):
        self.g.setAttribute("width", _u(width.conv("mm")))
        self.g.setAttribute("height", _u(height.conv("mm")))
        self.g.setAttribute("viewBox", "%s %s %s %s" % (0, 0, self.toVP(width), self.toVP(height)))
    
    def write(self):
        if self.path.suffix == ".svgz":
            f = GzipFile(str(self.path), mode="w")
        else:
            f = self.path.open("wb")

        xml = self.doc.toprettyxml( indent="  ", newl="\n", encoding="utf-8" )
        f.write(xml)
        f.close()


class TempSVG(SVG):
    def data(self):
        return self.doc.toprettyxml( indent="  ", newl="\n", encoding="utf-8" )


chessPattern = xml.dom.minidom.parseString( """\
<pattern id="chess" patternUnits="userSpaceOnUse" width="10" height="10" x="0" y="0">\
<rect x="0" y="0" width="5" height="5" fill="#fbfbfb" />\
<rect x="5" y="0" width="5" height="5" fill="#f7f7f7" />\
<rect x="0" y="5" width="5" height="5" fill="#f7f7f7" />\
<rect x="5" y="5" width="5" height="5" fill="#fbfbfb" />\
</pattern>""").documentElement