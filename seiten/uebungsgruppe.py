from common.files import cleanname
from common.locale import kurzesDatum
from auswertung.graphic import TextLabel, GraphicsItem
from auswertung.layout import *
from auswertung.ausdruck import Ausdruck
from auswertung.histogramm import *

def statLine(frage, text):
    l = HBoxLayout()
    l.addItem(TextLabel(40, 5, text))
    l.addItem(TextLabel(20, 5, frage.avgFormat("%.1f ± %.1f", "n.A.")))
    l.addItem(TextLabel(40, 5, "(%s)" % ", ".join("%s=%i" % (text, pos) for pos, text in frage.bedeutungen.items())))
    return l

def übungsgruppe(gruppe, folder, vorlesung, semester, evalu, dozent, ausdruck=None):
    gData = gruppe.data
    prefix = cleanname("ug_%s" % gruppe.value)
    
    a = ausdruck or Ausdruck(folder)
    a.addPage("%s_1" % prefix)
    
    vLayout = VBoxLayout(padding=10, margin=5)
    
    vLayout.addItem( TextLabel( 100, 10, "Ergebnisse der Beurteilung der Lehre durch Studierende",
                                        fontSize=5, align="center") )
    
    gl1 = GridLayout(6, 2, margin=0)
    gl1.setItem( TextLabel( 40, 5, "Vorlesung:" ), 0, 0 )
    gl1.setItem( TextLabel( 40, 5, "Semester" ), 1, 0 )
    gl1.setItem( TextLabel( 40, 5, "Übungsgruppenleiter/in:" ), 2, 0 )
    gl1.setItem( TextLabel( 40, 5, "Datum der Befragung:" ), 3, 0 )
    gl1.setItem( TextLabel( 40, 5, "Anzahl Bögen zurückbekommen:" ), 4, 0 )
    gl1.setItem( TextLabel( 40, 5, "gültige Bögen:" ), 5, 0 )
    
    gl1.setItem( TextLabel( 40, 5, vorlesung["titel"] ), 0, 1 )
    gl1.setItem( TextLabel( 40, 5, semester ), 1, 1 )
    gl1.setItem( TextLabel( 40, 5, gruppe.value), 2, 1 )
    gl1.setItem( TextLabel( 40, 5, kurzesDatum(evalu["termin"]) ), 3, 1 )
    gl1.setItem( TextLabel( 40, 5, str(gruppe.gesamt) ), 4, 1 )
    gl1.setItem( TextLabel( 40, 5, str(gData["31"].valid) ), 5, 1 )
    
    vLayout.addItem(gl1)
    
    sl1 = VBoxLayout()
    sl1.addItem(statLine(gData["22"], "22) Qualität Erklärungen?"))
    sl1.addItem(statLine(gData["23"], "23) Wie ist das Tempo?"))
    sl1.addItem(statLine(gData["24"], "24) Wie ist das Tafelbild?"))
    sl1.addItem(statLine(gData["25"], "25) Vorbereitung der Aufgaben?"))
    sl1.addItem(statLine(gData["26"], "26) Nachbereitung der Aufgaben?"))
    sl1.addItem(statLine(gData["27"], "27) Nachbereitung der Vorlesung?"))
    
    vLayout.addItem(sl1)
    
    l1 = GridLayout(2, 3, xmargin=5, ymargin=5)
    
    l1.setItem( gData["22"].graphic("22) Qualität Erklärungen"), 0, 0 )
    l1.setItem( gData["23"].graphic("23) Wie ist das Tempo"), 0, 1 )
    l1.setItem( gData["24"].graphic("24) Wie ist das Tafelbild"), 0, 2 )
    
    l1.setItem( gData["25"].graphic("25) Vorbereitung der Aufgaben"), 1, 0 )
    l1.setItem( gData["26"].graphic("26) Nachbereitung der Aufgaben"), 1, 1 )
    l1.setItem( gData["27"].graphic("27) Nachbereitung der Vorlesung"), 1, 2 )
    
    vLayout.addItem(l1)
    
    a.setLayout( vLayout )
    a.closePage()
    a.addPage("%s_2" % prefix)
    
    vLayout = VBoxLayout(padding=10, margin=5)
    
    vLayout.addItem( TextLabel(40, 5, "28) Was hast Du genutzt?") )
    l2 = GridLayout(1, 3, xmargin=5, ymargin=5)
    l2.setItem( gData["28.1"].graphic("Übungsgruppe"), 0, 0 )
    l2.setItem( gData["28.2"].graphic("Bearbeitung der Aufgaben"), 0, 1 )
    l2.setItem( gData["28.3"].graphic("Besprechen der Aufgaben\nmit anderen Studierenden"), 0, 2 )
    vLayout.addItem(l2)
    
    vLayout.addItem( TextLabel(40, 5, "28) Was hat zu Deinem Verständnis beigetragen?") )
    l3 = GridLayout(1, 3, xmargin=5, ymargin=5)
    l3.setItem( gData["28.4"].graphic("Übungsgruppe"), 0, 0 )
    l3.setItem( gData["28.5"].graphic("Bearbeitung der Aufgaben"), 0, 1 )
    l3.setItem( gData["28.6"].graphic("Besprechen der Aufgaben\nmit anderen Studierenden"), 0, 2 )
    vLayout.addItem(l3)
    
    vLayout.addItem( GraphicsItem(0, 10) )
    vLayout.addItem( TextLabel(40, 10, "Gesamtbeurteilung", fontSize=4, align="center") )
    
    hl = HBoxLayout()
    hl.addItem(TextLabel(120, 5, "31) Wie beurteilst Du die Übungsgruppe auf einer Punkteskala von 15-0?"))

    hl.addItem(TextLabel(20, 5, gData["31"].avgFormat("%.1f ± %.1f", "n.A.")))
    hl.addItem(TextLabel(30, 5, "(sehr gut=15, schlecht=0)"))
    vLayout.addItem(hl)
    
    subjektivNoten = gData["31"].stat
    hist = Histogramm("Wie beurteilst Du die Übungsgruppe auf einer Punkteskala von 15-0?",
        list( HistogrammEntry(value=subjektivNoten[i], label=str(i)) for i in range(15, -1, -1) )
    )
    
    vLayout.addItem( hist )
    
    a.setLayout( vLayout )
    a.closePage()

    return a