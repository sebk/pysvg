from auswertung.graphic import TextLabel, GraphicsItem
from auswertung.layout import *
from auswertung.ausdruck import Ausdruck
from auswertung.histogramm import Histogramm, HistogrammMultiEntry, HistogrammEntry
from common.locale import kurzesDatum


def fmt(o):
    if o is None: return "n.a."
    if isinstance(o, int): return str(o)
    if isinstance(o, float): return "%.2f" % o
    else: return str(o)


def aushang(auswertung, folder, vorlesung, semester, evalu, dozent, ausdruck=None):
    hInfo = auswertung.hauptauswertung.info
    hData = auswertung.hauptauswertung.data
    
    if not hInfo: return

    a = ausdruck or Ausdruck(folder)
    a.addPage("aushang_1")

    vLayout = VBoxLayout(padding=10, margin=5)

    vLayout.addItem( TextLabel( 100, 10, "Vorlesungsbewertung", fontSize=5, align="center") )
    vLayout.addItem( TextLabel( 100, 5, vorlesung["titel"]
                                        + (" — " + vorlesung["zusatz"] if vorlesung["zusatz"] else ""),
                               fontSize=4, align="center") )
    
    sl = StackedLayout()
    sl.addItem( TextLabel( 100, 5, "{vorname} {nachname}".format(**dozent), fontSize=4, align="center") )
    sl.addItem( TextLabel( 100, 5, semester, fontSize=4, align="right") )
    vLayout.addItem( sl )

    hl = HBoxLayout(margin=10)
    
    gl1 = GridLayout(4, 2, margin=0)
    gl1.setItem( TextLabel( 40, 4, "Datum der Befragung" ), 0, 0 )
    gl1.setItem( TextLabel( 40, 4, "Anzahl der Hörer/innen" ), 1, 0 )
    gl1.setItem( TextLabel( 40, 4, "Anzahl der Fragebögen" ), 2, 0 )
    gl1.setItem( TextLabel( 40, 4, "Rücklauf" ), 3, 0 )
    
    gl1.setItem( TextLabel( 20, 4, kurzesDatum(evalu["termin"]), align="right" ), 0, 1 )
    gl1.setItem( TextLabel( 20, 4, evalu["hörer"], align="right" ), 1, 1 )
    gl1.setItem( TextLabel( 20, 4, str(auswertung.gesamt), align="right" ), 2, 1 )
    
    try:
        rücklauf = "%.0f%%" % (100 * auswertung.gesamt / int(evalu["hörer"]))
    except ValueError:
        rücklauf = ""
    
    gl1.setItem( TextLabel( 20, 4, rücklauf, align="right"), 3, 1 )
    
    hl.addItem(gl1)
    
    vl2 = VBoxLayout()
    vl2.addItem( TextLabel( 60, 4, "Bewertung der Vorlesung", align="center" ) )
    gl2 = GridLayout(3, 2, xmargin=2)
    gl2.setItem( TextLabel( 40, 4, "Mittelwert:", align="right"), 0, 0)
    gl2.setItem( TextLabel( 20, 4, fmt(hInfo["wertung"]), align="left" ), 0, 1 )
    gl2.setItem( TextLabel( 40, 4, "Standardabweichung:" , align="right" ), 1, 0 )
    gl2.setItem( TextLabel( 20, 4, fmt(hInfo["wertung_s"]), align="left" ), 1, 1 )
    gl2.setItem( TextLabel( 40, 4, "Gültige Bögen:", align="right" ), 2, 0 )
    gl2.setItem( TextLabel( 20, 4, fmt(hInfo["gültig"]), align="left" ), 2, 1 )
    vl2.addItem(gl2)
    hl.addItem(vl2)
    vLayout.addItem( hl )

    l = GridLayout(4, 3, xmargin=5, ymargin=5)
    row = 0

    l.setItem( hData["1"].graphic("1) strukturiert"), row, 0 )
    l.setItem( hData["2"].graphic("2) begründet"), row, 1 )
    l.setItem( hData["3"].graphic("3) Stoffumfang"), row, 2 )
    row += 1

    l.setItem( hData["4"].graphic("4) Vorwissen"), row, 0 )
    l.setItem( hData["5.1"].graphic("5) Präsentation I"), row, 1 )
    l.setItem( hData["5.2"].graphic("5) Präsentation II"), row, 2 )
    row += 1

    l.setItem( hData["5.3"].graphic("5) Präsentation III"), row, 0 )
    l.setItem( hData["6"].graphic("6) Tempo"), row, 1 )
    l.setItem( hData["7.1"].graphic("7) Tafel- / Overheadbild I"), row, 2 )
    row += 1

    l.setItem( hData["7.2"].graphic("7) Tafel- / Overheadbild II"), row, 0 )
    l.setItem( hData["8.2"].graphic("8) Nutzen des Skripts"), row, 1 )
    l.setItem( hData["9.1"].graphic("9) Zwischenfragen gestellt"), row, 2 )
    row += 1

    vLayout.addItem( l )

    a.setLayout( vLayout )
    a.closePage()

    a.addPage("aushang_2")
    vLayout = VBoxLayout(padding=10, margin=5)

    l = GridLayout(4, 3, xmargin=5, ymargin=5)
    row = 0
    
    l.setItem( hData["9.2"].graphic("9) Zwischenfragen beantwortet"), row, 0 )
    l.setItem( hData["10"].graphic("10) Schwierigkeit der\nÜbungsaufgaben"), row, 1 )
    l.setItem( hData["11"].graphic("11) Zeitaufwand für\nÜbungsaufgaben", (2, 4, 6, 8) ), row, 2 )
    row += 1
    
    l.setItem( hData["12"].graphic("12) Wird nicht behand.\nStoff benötigt"), row, 0 )
    l.setItem( hData["13"].graphic("13) Aufgaben klar\nformuliert"), row, 1 )
    l.setItem( hData["14"].graphic("14) Interesse geweckt"), row, 2 )
    row += 1
    
    l.setItem( hData["18.1"].graphic("18) Nutzung von:\nVorlesung"), row, 0 )
    l.setItem( hData["18.2"].graphic("18) Nutzung von:\nNachbereitung Vorlesung"), row, 1 )
    l.setItem( hData["18.3"].graphic("18) Nutzung von:\nFachliteratur"), row, 2 )
    row += 1

    l.setItem( hData["18.4"].graphic("18) Beitrag zum Verständnis:\nVorlesung"), row, 0 )
    l.setItem( hData["18.5"].graphic("18) Beitrag zum Verständnis:\nNachbereitung Vorlesung"), row, 1 )
    l.setItem( hData["18.6"].graphic("18) Beitrag zum Verständnis:\nFachliteratur"), row, 2 )
    row += 1
    
    vLayout.addItem(l)
    
    objektivNoten = hInfo["objektivnoten"]
    subjektivNoten = hInfo["subjektivnoten"]
    hist = Histogramm("Bewertung",
        [HistogrammEntry(hInfo["bögen"] - hInfo["gültig"], label="ungültig", color="white")] \
        + \
        list(
            HistogrammMultiEntry(
                values=(subjektivNoten[i], objektivNoten[i]),
                label=str(i),
                colors=("gray", "black")
            ) for i in range(15, -1, -1)
        ),
        legend=Legend( [ ( "gray", "Frage 20" ), ( "black", "objektiv ¹" ) ] )
    )
    
    vLayout.addItem( hist )

    a.setLayout(vLayout)
    a.closePage()

    return a
