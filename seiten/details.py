from auswertung.graphic import TextLabel, GraphicsItem
from auswertung.layout import *
from auswertung.ausdruck import Ausdruck
from common.locale import kurzesDatum
from common.serialize import st2datetime
import common.fragebogen as fragebogen


def details(auswertung, folder, vorlesung, semester, evalu, dozent, ausdruck=None):
    hInfo = auswertung.hauptauswertung.info
    hData = auswertung.hauptauswertung.data
    
    if not hInfo: return
    
    a = ausdruck or Ausdruck(folder)
    a.addPage("details_1")
    
    vLayout = VBoxLayout(margin=5, padding=10)
    sl = StackedLayout()
    sl.addItem( TextLabel( 100, 5, vorlesung["titel"] + (" — " + vorlesung["zusatz"] if vorlesung["zusatz"] else "") ) )
    sl.addItem( TextLabel( 100, 5, "{vorname} {nachname}".format(**dozent), align="right") )
    
    vLayout.addItem(sl)
    
    vLayout.addItem( TextLabel(40, 10, "Angaben zur eigenen Person", fontSize=4, align="center") )

    hl1 = HBoxLayout(margin=5)
    vl2 = VBoxLayout()
    vl3 = VBoxLayout()
    hl4 = HBoxLayout(margin=0)
    hl5 = HBoxLayout(margin=0)
    hl4.addItem(GraphicsItem(10, 0, stretch=0))
    hl5.addItem(GraphicsItem(10, 0, stretch=0))
    gl6 = GridLayout()
    gl7 = GridLayout()
    
    hl4.addItem(gl6)
    hl5.addItem(gl7)
    vl2.addItem(TextLabel(80, 5, "34) Welches Studienziel hast Du?"))
    vl2.addItem(hl4)
    vl3.addItem(TextLabel(100, 5, "[1] Die Objektivnote setzt sich folgendermaßen zusammen:" ))
    vl3.addItem(hl5)
    hl1.addItem(vl2)
    hl1.addItem(GraphicsItem(10, 0, stretch=1))
    hl1.addItem(vl3)
    
    # left side
    row = 0
    for entryText, entryCount in hData["34"].stat.most_common():
        gl6.setItem( TextLabel(40, 5, "%s:" % entryText), row, 0)
        gl6.setItem( TextLabel(10, 5, "%i" % entryCount, align="right"), row, 1)
        row += 1
    
    row = 0
    for item in fragebogen.fragebogen01.items():
        if isinstance(item, fragebogen.Frage):
            wertung = item.attr.get("wertung")
            if not wertung: continue
            gl7.setItem( TextLabel( 20, 5, "Frage %s" % item.id, stretch=0), row, 0 )
            gl7.setItem( TextLabel( 10, 5, "%i%%" % wertung, stretch=0 ), row, 1 )
            row += 1
    
    vLayout.addItem(hl1)
    
    vLayout.addItem(GraphicsItem(0, 5))
    
    gl3 = GridLayout(xmargin=5, ymargin=5)
    
    gl3.setItem( hData["35"].graphic("35) In welchem Semester bist Du?"), 0, 0)
    gl3.setItem( hData["36"].graphic("36) Wieviele SWS Lehrveranstaltungen\nbesuchst Du dieses Semester?", range(5, 45, 5)), 0, 1 )
    vLayout.addItem(gl3)
    
    vLayout.addItem( hData["37.2"].graphic("37) Wieviele Stunden verbringst Du insgesamt pro Woche mit Fahrzeit", range(1,11)) )
    vLayout.addItem( hData["37.1"].graphic("37) Wieviele Stunden verbringst Du insgesamt pro Woche\n"
                                     "mit Deinem Studium an der Uni und zu Hause?",
                                     range(5, 75, 5)))
    
    
    a.setLayout(vLayout)
    a.closePage()

    return a