__author__ = 'sebk'

from auswertung.graphic import TextLabel
from auswertung.layout import *
from auswertung.histogramm import Histogramm, HistogrammMultiEntry, HistogrammEntry
from auswertung.svg import Color, mm
from common.locale import kurzesDatum

def fmt(o):
    if o is None: return "n.a."
    if isinstance(o, int): return str(o)
    if isinstance(o, float): return "%.2f" % o
    else: return str(o)


class ThinHist(Histogramm):
    binWidth = mm(3)
    binSpacing = mm(2)
    binHeight = mm(25)
    frame = False
    titleAlign = "left"
    labelSize = mm(2.2)


def combined(title, fragen, colors, names, labels=None):
    if labels is None:
        labels = fragen[0].bedeutungen

    entries = list()
    entries.append(
        HistogrammMultiEntry(list(f.empty for f in fragen), colors=list(c * 3. for c in colors), label="k.A.")
    )
    vmin = min(f.vmin for f in fragen)
    vmax = max(f.vmax for f in fragen)
    align = {vmin:"left", vmax:"right"}
    for key in range(vmin, vmax+1):
        entries.append( HistogrammMultiEntry(
            values=list(f.stat[key] for f in fragen),
            label=labels.get(key),
            colors=colors,
            labelAlign=align.get(key)
        ) )

    legend = Legend(list(zip(colors, names)), align="right")
    return ThinHist(title, entries, legend)

vlTitelFont = "Linux Libertine"
vlZusatzFont = "Linux Libertine Slanted"
dozentFont = "Linux Libertine Capitals"


def poster(auswertung, vorlesung, evalu, dozent):
    hInfo = auswertung.hauptauswertung.info
    hData = auswertung.hauptauswertung.data

    vLayout = VBoxLayout(padding=0, margin=3)

    vlTitel = VBoxLayout(margin=2)
    vlTitel.addItem(TextLabel(100, 5, vorlesung["titel"], fontSize=5, align="center", font=vlTitelFont))
    vlTitel.addItem(TextLabel(100, 4, vorlesung["zusatz"] or "", fontSize=4, align="center", font=vlZusatzFont))
    vLayout.addItem(vlTitel)
    vLayout.addItem( TextLabel(
        100, 5,
        "{titel} {vorname} {nachname}".format(**dozent),
        fontSize=5, align="center", font=dozentFont))

    gl = GridLayout(4, 4, xmargin=4)
    gl.setItem( TextLabel( 40, 4, "Datum der Befragung", align="right"), 0, 0 )
    gl.setItem( TextLabel( 40, 4, "Anzahl der Hörer/innen", align="right" ), 1, 0 )
    gl.setItem( TextLabel( 40, 4, "Anzahl der Fragebögen", align="right" ), 2, 0 )
    gl.setItem( TextLabel( 40, 4, "Rücklauf", align="right" ), 3, 0 )

    gl.setItem( TextLabel( 40, 4, kurzesDatum(evalu["termin"]), align="left" ), 0, 1 )
    gl.setItem( TextLabel( 40, 4, evalu["hörer"], align="left" ), 1, 1 )
    gl.setItem( TextLabel( 40, 4, str(auswertung.gesamt), align="left" ), 2, 1 )

    try:
        rücklauf = "%.0f%%" % (100 * auswertung.gesamt / int(evalu["hörer"]))
    except ValueError:
        rücklauf = ""

    gl.setItem( TextLabel( 40, 4, rücklauf, align="left"), 3, 1 )

    gl.setItem( TextLabel( 40, 5, "Bewertung der Vorlesung", align="right" ), 0, 2 )
    gl.setItem( TextLabel( 40, 5, "Mittelwert", align="right"), 1, 2)
    gl.setItem( TextLabel( 40, 5, fmt(hInfo["wertung"]), align="left" ), 1, 3 )
    gl.setItem( TextLabel( 40, 5, "Standardabweichung" , align="right" ), 2, 2 )
    gl.setItem( TextLabel( 40, 5, fmt(hInfo["wertung_s"]), align="left" ), 2, 3 )
    gl.setItem( TextLabel( 40, 5, "Gültige Bögen", align="right" ), 3, 2 )
    gl.setItem( TextLabel( 40, 5, fmt(hInfo["gültig"]), align="left" ), 3, 3 )
    vLayout.addItem( gl )

    l = GridLayout(xmargin=5, ymargin=0)
    row = 0

    l.setItem( hData["1"].graphic("1) strukturiert", ThinHist), row, 0 )
    l.setItem( hData["2"].graphic("2) begründet", ThinHist), row, 1 )
    l.setItem( hData["3"].graphic("3) Stoffumfang", ThinHist), row, 2 )
    l.setItem( hData["4"].graphic("4) Vorwissen\nvorhanden", ThinHist), row, 3 )
    row += 1

    l.setItem( hData["5.1"].graphic("5) Präsentation I", ThinHist), row, 0 )
    l.setItem( hData["5.2"].graphic("5) Präsentation II", ThinHist), row, 1 )
    l.setItem( hData["5.3"].graphic("5) Präsentation III", ThinHist), row, 2 )
    l.setItem( hData["6"].graphic("6) Tempo", ThinHist), row, 3 )
    row += 1

    l.setItem( hData["7.1"].graphic("7) Tafel- / Overheadbild I", ThinHist), row, 0 )
    l.setItem( hData["7.2"].graphic("7) Tafel- / Overheadbild II", ThinHist), row, 1 )
    l.setItem( hData["8.2"].graphic("8) Nutzen des Skripts", ThinHist), row, 2 )
    l.setItem( hData["9.1"].graphic("9) Zwischenfragen\ngestellt", ThinHist), row, 3 )
    row += 1

    l.setItem( hData["9.2"].graphic("9) Zwischenfragen\nbeantwortet", ThinHist), row, 0 )
    l.setItem( hData["10"].graphic("10) Schwierigkeit der\nÜbungsaufgaben", ThinHist), row, 1 )
    l.setItem( hData["11"].graphic("11) Zeitaufwand für\nÜbungsaufgaben", (2, 4, 6, 8), ThinHist), row, 2 )
    l.setItem( hData["12"].graphic("12) Wird nicht behand.\nStoff benötigt", ThinHist), row, 0 )
    l.setItem( hData["13"].graphic("13) Aufgaben klar\nformuliert", ThinHist), row, 1 )

    l.setItem( hData["14"].graphic("14) Interesse geweckt", ThinHist), row, 2 )
    row += 1

    vLayout.addItem(l)

    hl = HBoxLayout()
    c18a = Color(0, 1, 1) * 0.9
    c18b = Color(1, 0, 1) * 0.9
    c18c = Color(1, 1, 0) * 0.9
    hl.addItem(combined("18) Nutzung von …",
        (hData["18.1"], hData["18.2"], hData["18.3"]),
        (c18a, c18b, c18c),
        ("Vorlesung", "Nachbereitung", "Fachliteratur")))

    hl.addItem(combined("18) Beitrag zum Verständnis …",
        (hData["18.4"], hData["18.5"], hData["18.6"]),
        (c18a, c18b, c18c),
        ("Vorlesung", "Nachbereitung", "Fachliteratur")))
    vLayout.addItem(hl)

    objektivNoten = hInfo["objektivnoten"]
    subjektivNoten = hInfo["subjektivnoten"]
    c20 = Color(r=1.0, g=0.5)
    cSubj = Color(b=1) * 1.5
    hist = ThinHist("Bewertung",
        [HistogrammEntry(hInfo["bögen"] - hInfo["gültig"], label="ungültig", color="white")] \
        + \
        list(
            HistogrammMultiEntry(
                values=(subjektivNoten[i], objektivNoten[i]),
                label=str(i),
                colors=(c20, cSubj)
            ) for i in range(15, -1, -1)
        ),
        legend=Legend( [ ( c20, "Frage 20" ), ( cSubj, "objektiv ¹" ) ] )
    )

    vLayout.addItem( hist )

    return vLayout