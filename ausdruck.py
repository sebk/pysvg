from .svg import SVG, TempSVG, chessPattern, A4


class Ausdruck:
    debug = False
    
    def __init__(self, folder):
        self.folder = folder
        self.svg = None
        self.layout = None
        self.pages = list()
    
    def addPage(self, name, size=A4):
        assert self.svg is None
        
        self.width, self.height = size
        self.svg = SVG(self.folder / ("%s.svg" % name))

    
    def setLayout(self, layout):
        self.layout = layout

    def draw(self):
        assert self.svg
        assert self.layout

        if self.width is None:
            self.width = self.layout.width

        if self.height is None:
            self.height = self.layout.height

        self.svg.setSize(self.width, self.height)
        if self.debug:
            self.svg.g.appendChild(chessPattern)
            self.svg.rect(0, 0, self.width, self.height, fill="url(#chess)", stroke="none")

        self.layout.draw(self.svg, self.width, self.height)

    def closePage(self):
        self.draw()
        self.svg.write()
        self.pages.append(self.svg)

        self.svg = None


class TempAusdruck(Ausdruck):
    def __init__(self):
        super().__init__(None)

    def addPage(self, name, size=A4):
        assert self.svg is None

        self.width, self.height = size
        self.svg = TempSVG(None)

    def closePage(self):
        self.draw()
        self.pages.append(self.svg)
        self.svg = None
